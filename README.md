# README #


### What is this repository for? ###

This dataset of behavior trees and state machines is an online appendix for the following papers. Please cite paper A if you use or analyze the dataset.

- (paper A) Behavior Trees and State Machines in Robotics Applications (https://doi.org/10.1109/TSE.2023.3269081), published at IEEE Transaction in Software Engineering (TSE), archived version available at https://www.cse.chalmers.se/~bergert/paper/2023-tse-behaviortrees.pdf

- (paper B) Behavior Trees in Action: A Study of Robotics Applications (https://dl.acm.org/doi/10.1145/3426425.3426942).

### How do I get set up? ###
In the [2022-filtered_models_dataset](2022-filtered_models_dataset) you can find the final dataset after applying the different filtering steps mentioned in figure 5 in paper A. 

In the [2022-sampled-dataset](2022-sampled-dataset) you can find the randomly sampled dataset used in paper A analysis.

We have structured both former mentioned folders as two seperate folders each for a dataset: behavior trees dataset and state machines dataset. In the "raw-model-data" folder in each dataset folder, you can find the codes that include the behavior model in a project.

For the statistics that are aggregated and presented in both papers, we provide an excel file for each behavior modeling languague dataset with detailed stats about each model. Finally, all the codes mentioned in our papers and used for mining projects and analysis is provided in the [scripts folder](scripts).

### Who do I talk to? ###

* Razan Ghzouli (razan.ghzouli@chalmers.se)
* Thorsten Berger (thorsten.berger@rub.de)
* Andrzej Wasowski (wasowski@itu.dk)
* Einar Broch Johnsen (einarj@ifi.uio.no)


